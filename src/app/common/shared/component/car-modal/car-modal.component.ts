import { Inject, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as moment from 'moment';

import { CanstarService } from '../../../core/service/canstar.service';

@Component({
  selector: 'app-car-modal',
  templateUrl: './car-modal.component.html',
  styleUrls: ['./car-modal.component.scss']
})
export class CarModalComponent implements OnInit {

  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(FormBuilder) public fb: FormBuilder,
    private ref: MatDialogRef<CarModalComponent>,
    private canstar: CanstarService
  ) {
    this.form = fb.group({
      'days': [ '' ],
    })
  }

  ngOnInit() {
    console.log(this.data);
  }

  bookNow() {
    this.canstar.bookARent({
      reg_uid: this.data.uid,
      own_rating: 0,
      timestamp: moment().unix(),
      num_days_rented: this.form.value.days,
      rent_per_day: this.data.car_data.rent_per_day,
      total_rent: this.form.value.days*this.data.car_data.rent_per_day,
      modes_of_payment: 'cash'
    })
    this.ref.close();
  }

}
