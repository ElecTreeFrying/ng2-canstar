import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class EntryGuard implements CanActivate, CanActivateChild {

  constructor(
    private http: HttpClient,
    private fire: AngularFireAuth
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    this.fire.authState.pipe(
      map((user: any) => {
        const email = user.email;
        this.http.get('assets/api/people.json').subscribe((res: any[]) => {
          const currentUser = res.filter((e) => {
            return e.email.toLowerCase() === email
          })[0]
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          this.http.get('assets/api/registered-users.json').subscribe((rrr: any[]) => {
            const currentUser2 = rrr.filter((e) => {
              return e.id === currentUser.id
            })[0]
            localStorage.setItem('currentUser2', JSON.stringify(currentUser2));
          });
          this.http.get('assets/api/user-bookings.json').subscribe((res: any[]) => {
            const userBookings = res.filter(e => e.id === currentUser.id);
            localStorage.setItem('userBookings', JSON.stringify(userBookings));
          });
        });


      })
    ).subscribe(() => 0);

    this.fire.authState.pipe(
      map((user: any) => {
        return user !== null
      })
    ).subscribe(() => 0);

    return this.fire.authState.pipe(
      map((user: any) => user !== null)
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

}

@Injectable({
  providedIn: 'root'
})
export class ExitGuard implements CanActivate, CanActivateChild {

  constructor(
    private fire: AngularFireAuth
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    this.fire.authState.pipe(
      map((user: any) => {
        return user === null
      })
    ).subscribe(() => 0);

    return this.fire.authState.pipe(
      map((user: any) => user === null)
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

}
