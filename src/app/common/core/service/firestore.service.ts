import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(
    private db: AngularFirestore
  ) { }

  updateId(rent: any) {
    const id = JSON.parse(localStorage.getItem('currentUser')).id;
    let i = 0;
    this.db.collection('user-bookoings', (e) => {
      return e.where('id', '==', id)
    }).snapshotChanges().pipe(
      map((values) => {
        return values.map((fire) => {
          const doc = fire.payload.doc;
          if (doc.data()['id'] === id) {
            const old = <any[]>doc.get('books')
            old.push(rent)
            if (i === 0) {
              doc.ref.update({
                books: old
              })
              i++;
            }
          }
        })
      })
    ).subscribe(() => 0);
  }

  get readBooks() {
    const id = JSON.parse(localStorage.getItem('currentUser')).id;
    return this.db.collection('user-bookoings', (e) => {
      return e.where('id', '==', id)
    }).snapshotChanges().pipe(
      map((values) => {
        return values.map((fire) => {
          return { ...fire.payload.doc.data() }
        })
      })
    );
  }


  pushOne(data: any) {
    this.db.collection('user-bookoings').add(data);
  }

  goOnline() {
    this.db.firestore.enableNetwork().then(() => {
    }).catch(() => {
    })
  }

  goOffline() {
    this.db.firestore.disableNetwork().then(() => {
    }).catch(() => {
    })
  }

}
