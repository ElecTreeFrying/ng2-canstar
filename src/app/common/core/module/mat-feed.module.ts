import { NgModule } from '@angular/core';
import {
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatDividerModule,
  MatRippleModule,
  MatProgressSpinnerModule,
  MatDialogModule,
} from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  exports: [
    ScrollingModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    NgbRatingModule,
    MatDialogModule,
  ]
})
export class MatFeedModule { }
