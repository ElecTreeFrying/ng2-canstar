import { Component, OnInit } from '@angular/core';

import { FirestoreService } from '../../../common/core/service/firestore.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user: any;
  user2: any;
  userBookings: any;

  constructor(
    private firestore: FirestoreService
  ) { }

  ngOnInit() {
    const currentUser = localStorage.getItem('currentUser');
    const currentUser2 = localStorage.getItem('currentUser2');

    this.user = JSON.parse(currentUser)
    this.user2 = JSON.parse(currentUser2)
    this.userBookings = this.firestore.readBooks

  }

}
