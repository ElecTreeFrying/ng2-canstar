import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { ProfileRoutingModule } from './profile-routing.module';
import { MatProfileModule } from '../../../common/core/module/mat-profile.module';

import { ProfileComponent } from './profile.component';
import { NgbdRatingDecimal } from './rating-decimal';

@NgModule({
  declarations: [
    ProfileComponent,
    NgbdRatingDecimal,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbRatingModule,
    ProfileRoutingModule,
    MatProfileModule
  ]
})
export class ProfileModule { }
